<!DOCTYPE html>
<html>
    <head>
	    <title><?php echo "$page_title";?></title>
	    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	    <link rel="stylesheet" type="text/css" href="css/material.css">
	    <link rel="stylesheet" type="text/css" href="css/style.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	    <script type="text/javascript" src="js/material.min.js"></script>
    </head>
    <body>