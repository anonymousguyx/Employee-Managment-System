<?php 
/**
*  Made by Zeeshan Ahmed
*/
class Session
{
	private $m_username = "admin";
	private $m_password = "admin";

	const TYPE_MANAGER = 1;
	const TYPE_EMPLOYEE = 2;

	function is_valid_user($data)
	{
		if ($data["username"] == $this->m_username && $data["password"] == $this->m_password) {
				$_SESSION["usertype"]= "1";
			}
		else
		{
			echo "Invalid User";
			exit();
		}
	}

	function login($data)
	{
		session_start();
		$this->is_valid_user($data);
		$_SESSION["usertype"] = $data["usertype"];
		$_SESSION["username"] = $data["username"];
		header("Location: ./index.php?ref=dashboard");
	}

	function logout()
	{
		session_start();
		unset($_SESSION["usertype"]);
		unset($_SESSION["username"]);
		session_destroy();
	}
}
 ?>