<?php 
/**
*  Created by Zeeshan Ahmed
*/
include "model/session.php";
class User
{
	
	function render($pagename,$page_title){
        include_once("./template/header.php");
        include_once("$pagename.php");
        include_once("./template/footer.php");
    }
    function login()
	{
		$data=array
		(
			"username" => isset($_POST["username"])? $_POST["username"] : "",
			"password" => $_POST["password"]
		);
		$obj=new Session();
        $obj->login($data);
	}

	function logout()
	{
		$obj = new Session();
		$obj->logout();
		header("Location: ./index.php?ref=login");
	}

	function login_page()
	{
        $this->render("login","LOGIN - Employee Leave Management");
	}

	function dashboard()
	{
		session_start();
		if (isset($_SESSION["username"])) {
			$this->render("dashboard","Employee Dashboard");
		}
		else {
			header("Location: ./index.php?ref=login");
		}
	}
    
}
 ?>