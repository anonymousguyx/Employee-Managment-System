
		<div class="mdl-grid mdl-grid--no-spacing">
			<div class="mdl-cell--stretch mdl-cell--6-col mdl-cell--3-col-tablet mdl-cell--hide-phone empimg">		
		    </div>
		    <div class="mdl-cell--stretch mdl-cell--6-col mdl-cell--5-col-tablet mdl-cell--12-col-phone login-p">
			    <h3 class="mdl-typography--font-thin mdl-typography--text-center info-h">
				     Please login
			    </h3>
			    <p class="mdl-typography--text-center">
				    Please login with your credentials to get started.
			    </p>
			    <form id="logn_form" action="index.php?ref=is_login" method="POST" class="mdl-typography--text-center">
	           	    <div class="mdl-textfield mdl-js-textfield">
	               	    <input class="mdl-textfield__input" name="username" type="text" id="username">
	               	    <label class="mdl-textfield__label" for="username">Username</label>
	         	    </div>
	        	    <br />
	        	    <div class="mdl-textfield mdl-js-textfield">
	            	    <input class="mdl-textfield__input" name="password" type="password" id="password">
		           	    <label class="mdl-textfield__label" for="password">Password</label>
	        	    </div>
	        	    <br />
	        	    <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary" type="submit" value="Login">
				      LOGIN
				    </button>
      		    </form>
		    </div>
		</div>