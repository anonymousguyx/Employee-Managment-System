<?php 

include_once ("./controller/user.php");
	function get_get($action = "")
	{
		return $_GET[$action];
	}
    
	function main()
	{
		$obj = new User();
        switch (get_get("ref")) {
			case 'login':
				$obj->login_page();
				break;
			case 'is_login':
				$obj->login();
				break;
			case 'logout':
				$obj->logout();
				break;
			case 'dashboard':
				$obj->dashboard();
				break;
			default:
                $obj->login_page();
				break;
		}
	}
main();
 ?>
